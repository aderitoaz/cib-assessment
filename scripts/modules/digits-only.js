(function () {
  var _allowedKeys = [
    "Backspace",
    "ArrowUp",
    "ArrowDown",
    "ArrowLeft",
    "ArrowRight",
  ];
  /** @param { HTMLInputElement}  inputElement*/
  function apply(inputElement) {
    var el = inputElement;
    el.addEventListener("keydown", function (event) {
      /**
       * This is not complete.
       * There's further work to be done to support IE and some other characters and combination characters (pasting, cutting, etc.)
       * I've always seen lots of conditions not being catered for with online financial tools from banking sites.
       */
      var key = event.key;
      if (!key.match(/\d/) && !_hasAllowedKey(key)) {
        event.preventDefault();
      }
    });
  }

  function _hasAllowedKey(key) {
    return _allowedKeys.includes(key);
  }

  window.DigitsOnly = {
    apply: apply,
  };
})();

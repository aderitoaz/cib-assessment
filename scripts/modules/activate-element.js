(function () {
  /** @param el { HTMLElement} */
  function registerChildren(el) {
    var children = Array.from(el.children);
    children.forEach(function (child) {
      child.addEventListener("click", function (event) {
        children.forEach(function (event) {
          event.classList.remove("active");
        });
        this.classList.add("active");
      });
    });
  }

  var canActivateElements = Array.from(
    document.querySelectorAll("[can-activate]")
  );

  window.ActivateElement = {
    registerChildren: registerChildren,
  };

  canActivateElements.forEach(function (el) {
    registerChildren(el);
  });
})();

/** @todo need to check that return date is after departure date */
(function () {
  var container;
  var currentDate = new Date();
  var selectedDate = null;
  var _onClose;
  var monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  function createFullScreenContainer() {
    container = document.createElement("div");
    container.classList.add("fullscreen-overlay");
  }
  function calendar(onClose) {
    _onClose = onClose;
    createFullScreenContainer();
    insertCalendar();
  }

  function insertCalendar() {
    var now = currentDate;
    var calendar = document.createElement("div");
    calendar.classList.add("calendar");
    calendar.appendChild(
      _getMonthPicker(now.getFullYear(), monthNames[now.getMonth()])
    );
    calendar.appendChild(
      _getDayPicker(currentDate.getFullYear(), currentDate.getMonth())
    );
    calendar.appendChild(_getFooter());

    container.appendChild(calendar);
    document.body.appendChild(container);
    ActivateElement.registerChildren(document.querySelector(".calendar ul"));
  }

  function _getMonthPicker(year, month) {
    var picker = document.createElement("div");

    var left = document.createElement("div");
    var center = document.createElement("div");
    var right = document.createElement("div");

    left.innerHTML = '<i class="fas fa-chevron-left"></i>';
    left.addEventListener("click", leftChevronClick);

    center.innerText = month + ", " + year;

    right.innerHTML = '<i class="fas fa-chevron-right"></i>';
    right.addEventListener("click", rightChevronClick);

    picker.appendChild(left);
    picker.appendChild(center);
    picker.appendChild(right);
    return picker;
  }

  function rightChevronClick() {
    document.querySelector(".calendar").remove();
    currentDate.setMonth(currentDate.getMonth() + 1);
    insertCalendar();
  }

  function leftChevronClick() {
    document.querySelector(".calendar").remove();
    currentDate.setMonth(currentDate.getMonth() - 1);
    insertCalendar();
  }

  function _getDayPicker(year, month) {
    var picker = document.createElement("ul");

    picker.addEventListener("click", function (event) {
      selectedDate = new Date(event.target.getAttribute("data"));
    });

    const firstSunday = getFirstSunday(year, month);

    for (let i = 0; i < 28; i++) {
      var li = document.createElement("li");
      var date = new Date(
        year,
        firstSunday.getMonth(),
        firstSunday.getDate() + i
      );
      li.innerText = date.getDate();
      li.setAttribute("data", date);
      picker.appendChild(li);
    }

    return picker;
  }

  function _getFooter() {
    var footer = document.createElement("div");
    footer.classList.add("footer");

    var ok = document.createElement("button");
    var cancel = document.createElement("button");

    ok.innerText = "ok";
    cancel.innerText = "cancel";

    ok.addEventListener("click", function () {
      _onClose(selectedDate);
      document.querySelector(".fullscreen-overlay").remove();
    });

    cancel.addEventListener("click", function () {
      _onClose(null);
      document.querySelector(".fullscreen-overlay").remove();
    });

    footer.appendChild(ok);
    footer.appendChild(cancel);

    return footer;
  }

  function getFirstSunday(year, month) {
    var date = new Date(year, month, 1);
    while (date.getDay() !== 0) {
      date.setDate(date.getDate() - 1);
    }

    return date;
  }

  window.Calendar = {
    calendar: calendar,
  };
})();

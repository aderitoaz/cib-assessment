/** Not using any es6 syntax to increase browser support */
(function () {
  var data;
  var _destinationMap;
  DOM.navLeft.currency.addEventListener("click", function () {
    var dropdown = window.DOM.getCurrencyDropDown(
      data.currencies.map(function (currency) {
        return currency.description + " (" + currency.short + ")";
      }),
      function (index) {
        dropdown.remove();
        DOM.navLeft.currency.querySelector("span").innerText =
          data.currencies[index].short;
      }
    );
    document.body.appendChild(dropdown);
  });

  function setDestinationPickerInput(input) {
    var dropdown;
    /** @type { HTMLInputElement} */
    input.addEventListener("focus", function () {
      var searchText = input.value.toLowerCase().trim();
      onSearch(searchText);
    });

    input.addEventListener("keyup", function (event) {
      var searchText = input.value.toLowerCase().trim();
      dropdown.remove();
      onSearch(searchText);
    });

    function onSearch(text) {
      dropdown = window.DOM.getDestinationDropDown(
        _destinationMap,
        text,
        function (index) {
          input.value = _destinationMap[index];
          dropdown.remove();
        }
      );
      input.parentElement.appendChild(dropdown);
    }
  }

  function setDatePickerInput(input) {
    input.addEventListener("click", function () {
      Calendar.calendar(function (date) {
        if (date) {
          input.value =
            Number(date.getMonth() + 1) +
            "/" +
            date.getDate() +
            "/" +
            date.getFullYear();
        }
      });
    });
  }

  setDestinationPickerInput(DOM.form.fromInput);
  setDestinationPickerInput(DOM.form.toInput);

  setDatePickerInput(DOM.form.departureInput);
  setDatePickerInput(DOM.form.returnInput);

  DigitsOnly.apply(DOM.form.adultInput);

  fetch("/assets/data.json")
    .then(function (response) {
      response.json().then(function (json) {
        data = json;
        _destinationMap = json.destinations.map(function (destination) {
          return destination.city + ", " + destination.country;
        });
      });
    })
    .catch(function () {
      // error handling 😎
    });
})();

/**
 * In an ideal world these would be exposed and discoverable through an API
 * However, trying to increase browser support by not using `import` syntax
 **/
(function () {
  function $(selector) {
    return document.querySelector(selector);
  }

  /** Nav Left */
  var navLeft = {
    // home: $(""),
    // faqs: $(""),
    // contact: $(""),
    currency: $(".nav-left li:nth-child(4)"),
    // currencyDropDown: $(""),
  };

  /** Nav Right */
  var navRight = {
    // home: $(""),
    // flights: $(""),
    // hotels: $(""),
    // holidays: $(""),
    // blog: $(""),
    // phone: $(""),
  };

  /** Nav Social */

  var navSocial = {
    //   facebook: $(""),
    //   twitter: $(""),
    //   pinterest: $(""),
    //   instagram: $(""),
    //   phone: $(""),
    //   email: $(""),
  };
  // /** Nav Title */
  var navTitle = {
    //   // logo: $(""),
  };

  /** Nav Form */
  var navForm = {
    //   flight: $(""),
    //   hotels: $(""),
    //   flightAndHotels: $(""),
    //   holidays: $(""),
  };
  // /** Form */
  var form = {
    //   // title: $(""),
    //   // domesticBtn: $(""),
    //   // internationalBtn: $(""),
    fromInput: $(".destinations .field:first-child input"),
    toInput: $(".destinations .field:last-child input"),
    departureInput: $(".dates > .field:nth-child(1) > input"),
    returnInput: $(".dates > .field:nth-child(2) > input"),
    adultInput: $(".dates > .field:nth-child(3) > input"),
    //   // searchBtn: $(""),
    //   // advancedSearchBtn: $(""),
  };

  /** @param currencies { string[] } */
  function getCurrencyDropDown(currencies, onCurrencySelect) {
    var container = document.createElement("div");
    container.classList.add("fullscreen-overlay");
    var ul = document.createElement("ul");
    ul.classList.add("currency-dropdown");
    currencies.forEach(function (currency, index) {
      var li = document.createElement("li");
      li.innerText = currency;
      ul.appendChild(li);
      li.addEventListener("click", function () {
        onCurrencySelect(index);
      });
    });
    container.appendChild(ul);
    return container;
  }

  /** @param destinations {string [] } */
  function getDestinationDropDown(
    destinations,
    searchText,
    onDestinationSelect
  ) {
    var container = document.createElement("div");
    container.classList.add("destination-dropdown-container");
    var ul = document.createElement("ul");
    ul.classList.add("destination-dropdown");
    var regex = new RegExp(searchText.toLowerCase().trim(), "i");
    destinations.forEach(function (destination, index) {
      if (regex.test(destination)) {
        var li = document.createElement("li");
        li.innerText = destination;
        ul.appendChild(li);

        li.addEventListener("click", function () {
          onDestinationSelect(index);
        });
      }
    });
    container.appendChild(ul);
    return container;
  }

  window.DOM = {
    $: $,
    navLeft: navLeft,
    navRight: navRight,
    navSocial: navSocial,
    navTitle: navTitle,
    navForm: navForm,
    form: form,
    getCurrencyDropDown: getCurrencyDropDown,
    getDestinationDropDown: getDestinationDropDown,
  };
})();

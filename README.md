
## Getting started

Run `npm install` and then `npm start`

## My other works
- https://sarapapa.design (CSS)

- https://mzansilingos.com (DART) - converts to HTML/CSS/JS

## Previews

<img src='./misc/1.png' width='800'>
<img src='./misc/2.png' width='800'>
<img src='./misc/3.png' width='800'>
<img src='./misc/4.png' width='800'>